#!/usr/bin/env python

#############################################################################
#
# Copyright 2021, Erik Ridderby
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this 
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation and/or 
# other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
#############################################################################
#
# This script move files in a monitored folder but not before the file is
# of an given age (in seconds). 
# A directory is monitored by polling and the polling frequency can be 
# adjusted. 
#
#

import os
import time

def monitor_files( basepath,  target, time_limit=20, check_interval=5 ):
    now = time.time()
    
    # Select one of those by commenting out the other 
    # (remove the # on the one to use and add it to the
    # the other)
    #
    # Use this if run on Linux, BSD, Unix, Solaris
    PathDilimitor = "/"
    #
    # Use this if run on Windows or DOS
    # PathDilimitor = "\\"
    
    while(True):
        print("Check files")
        actual_limit = time.time() - time_limit;
        # print(actual_limit)
        for filename in os.listdir(basepath):
            print("Got file: " + filename)
            filepath = basepath + PathDilimitor + filename            
            # print(os.path.getmtime(filepath))
            if os.path.exists( filepath ) and os.path.isfile(filepath) and os.path.getmtime(filepath) < actual_limit:
                target_file_path = target + PathDilimitor + filename
                print("Move file: " + filepath + " to " + target_file_path)
                os.rename(filepath, target_file_path)
        
        # Wait for check interval seconds, then check again.
        time.sleep( check_interval )
                
                
                
                
if __name__ == '__main__':
    
    # Paths works different in Linux and Unix
    # Example path with windows backslash
    # monitorPath = "c:\\data\\spource"
    #
    # Example path for Linux, BSD, Unix, Solaris using front slash
    # monitorPath = "/var/tmp/source"
    
    ###
    # Settings for the script
    #
    # Setup the path that shall be monitored
    monitorPath = "./source"
    #
    # Setup the path where files shall be moved to
    moveToPath  = "./target"
    #
    # Set the least age in seconds of files before they are moved
    time_limit = 20
    #
    # Set the time in seconds between polling, 
    # the script will sleep this long between goes 
    check_interval = 5

    monitor_files(monitorPath, moveToPath, time_limit, check_interval)

